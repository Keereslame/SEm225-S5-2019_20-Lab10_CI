#include "integer_avg.h"

/* Function that return the average of two integers */
int integer_avg(int a, int b)
{
    return (a + b)/2;
}